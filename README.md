# beardbolt

An experimental fork of [rmsbolt](https://gitlab.com/jgkamat/rmsbolt),
itself a supercharged implementation of [godbolt
compiler-explorer](https://github.com/mattgodbolt/compiler-explorer)
for Emacs.

beardbolt tries to make it easy to see what your compiler is doing.
It does this by showing you the assembly output of a given source code
file.  It also highlights which source code a given assembly block
corresponds to, and vice versa.

### Why rmsbolt over beardbolt

- Supports more languages/compilers. beardbolt only C++/C clang/gcc.
- Has good documentation and a proper API.
- Supports more Emacs versions.  beardbolt only 28+
- Support compile-commands.json

### Why beardbolt over rmsbolt

- Doesn't require file to be saved.
- Faster (2x) and more responsive (see [here][#benchmarks])
- Less buggy (TODO: show actual rmsbolt problems)
- Has rainbows.
- Has `beardbolt-preserve-library-functions` switch like godbolt.
- Simpler code (half the LOC)

### Installation

```sh
cd /path/to/beardbolt/clone
make
```

```lisp
(add-to-list 'load-path "/path/to/beardbolt/clone")
(require 'beardbolt)
```

```
M-x beardbolt-starter
```

<a name="benchmarks"></a>
### Benchmarks vs rmsbolt

First, a word on what "fast" means.  The performance metric to
optimize is responsiveness, i.e. having the extension provide its
service as fast as possible, intruding as little as possible in the
user experience.

Also note the beardbolt is highly hacky/experimental and it may be
providing incorrect results, in which case most/all of this comparison
is invalid.

Anyway both beardbolt and rmsbolt continuously analyze the buffer to present
a "live" view of its assembly state. They do this work in a two step
process:

1. the file being worked on is partially compiled by an external program
2. and then some processing takes place on the assembly output.

The first step happens asynchronously, but the second one is done in
Elisp.  If it takes a long time, this is generally bad.  While it can
be made less annoying (by using something like `while-no-input`, which
beardbolt uses and rmsbolt _could_ probably use) it is nevertheless
noticeable.  Therefore, it is important the second phase run as fast
as possible.

#### Results

To run the benchmarks, have both rmsbolt and beardbolt clones
side-by-side, then:

```
cd /path/to/beardbolt/clone
make benchmark
```

This will run `beardbolt-compile` and `rmsbolt-compile` 10 times on a
known "problematic file" (found
[here](https://gitlab.com/jgkamat/rmsbolt/-/issues/9) in rmsbolt's bug
tracker).  At time of writing these are the results on my Thinkpad T480
running Emacs 29 (without native compilation):

```
$ EMACS=~/Source/Emacs/emacs/src/emacs make benchmark
Beardbolt on slow-to-process.cpp took 2.342437596s per run
Rmsbolt on slow-to-process.cpp took 4.995786035500001s per run
```
