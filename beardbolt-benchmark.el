(require 'beardbolt)
(require 'cl-lib)
(require 'benchmark)

(defvar beardbolt-benchmark-samples nil)
(defvar rmsbolt-benchmark-samples nil)
(defconst repeats 10)

(advice-add (quote beardbolt--handle-finish-compile) :around
            (lambda (oldfun &rest args)
              (push (benchmark-elapse (apply oldfun args))
                    beardbolt-benchmark-samples)))

(with-current-buffer
    (find-file (car argv))
  (cl-loop repeat repeats
           do (beardbolt-compile (beardbolt--get-lang))
           (while (process-live-p
                   (get-buffer-process (beardbolt--compilation-buffer)))
             (accept-process-output)))
  (message "Beardbolt on %s took %ss per run"
           (file-name-nondirectory buffer-file-name)
           (/ (cl-reduce #'+ beardbolt-benchmark-samples)
              (length beardbolt-benchmark-samples) 1.0))
  (add-to-list 'load-path (expand-file-name "../../rmsbolt" default-directory))
  (when (require 'rmsbolt nil t)
    (advice-add (quote rmsbolt--handle-finish-compile) :around
                (lambda (oldfun &rest args)
                  (push (benchmark-elapse (apply oldfun args))
                        rmsbolt-benchmark-samples)))
    (cl-loop
     repeat repeats
     do
     (sit-for 0.5) ;; rmsbolt needs to do some state cleanup, apparently
     (rmsbolt-compile)
     (while (process-live-p
             (get-buffer-process (get-buffer "*rmsbolt-compilation*")))
       (accept-process-output)))
    (message "Rmsbolt on %s took %ss per run"
             (file-name-nondirectory buffer-file-name)
             (/ (cl-reduce #'+ rmsbolt-benchmark-samples)
                (length rmsbolt-benchmark-samples) 1.0))))




